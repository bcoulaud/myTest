package com.bapt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StartBoot {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(StartBoot.class, args);
	}
}
