package com.bapt.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Service;

import com.bapt.dto.UserDto;

@Service
public class UserService implements InitializingBean {

	private Map<String, UserDto> users;

	@Override
	public void afterPropertiesSet() throws Exception {
		users = new HashMap<>();
	}

	public List<UserDto> get() {
		return users.entrySet().stream().map(Entry::getValue).collect(Collectors.toList());
	}

	public UserDto getById(String id) {
		return users.getOrDefault(id, null);
	}

	public UserDto create(UserDto request) {
		users.put(request.getLogin(), request);
		return request;
	}

	public void delete(String id) {
		if (users.containsKey(id)) {
			users.remove(id);
		}
	}

}
